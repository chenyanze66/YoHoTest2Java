package com.cyz.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

/**
 * 创建日期：2020/12/24 11:01
 *
 * @author cyz
 * 包名： com.cyz.greendao
 * 类说明：
 */
@Entity(nameInDb = "STUDENTBEAN")
public class StudentBean {
    @Id(autoincrement = true)
    @Unique
    private Long id;
    @Property(nameInDb = "name")
    private String name;
    @Property(nameInDb = "pwd")
    private String pwd;

    @Generated(hash = 1172200404)
    public StudentBean(Long id, String name, String pwd) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
    }
    @Generated(hash = 2097171990)
    public StudentBean() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPwd() {
        return this.pwd;
    }
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "StudentBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pwd='" + pwd + '\'' +
                '}';
    }
}

package com.cyz.greendao;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import com.dao.green.db.DaoMaster;
import com.dao.green.db.DaoSession;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DaoMaster.DevOpenHelper abc = new DaoMaster.DevOpenHelper(this, "abc");
        SQLiteDatabase writableDatabase = abc.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(writableDatabase);
        DaoSession daoSession = daoMaster.newSession();
        StudentBean 陈延泽 = new StudentBean(2L, "雷金龙", "123123");
//        daoSession.insert(陈延泽);
        daoSession.update(陈延泽);

        List<StudentBean> studentBeans = daoSession.loadAll(StudentBean.class);
        Log.e("cyz", "onCreate: "+studentBeans.toString() );
    }
}
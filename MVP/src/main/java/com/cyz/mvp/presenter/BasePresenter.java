package com.cyz.mvp.presenter;

import com.cyz.mvp.model.IModel;
import com.cyz.mvp.ui.IView;

import java.lang.ref.SoftReference;

/**
 * 创建日期：2020/12/14 14:14
 *
 * @author cyz
 * 包名： com.cyz.mvp.presenter
 * 类说明：
 */
public abstract class BasePresenter<M extends IModel , V extends IView> {
    protected SoftReference<V> mView;
    protected M mModel;

    public BasePresenter(V _View) {
        mView = new SoftReference<>(_View);
        mView.get();
        setModel();



    }

    protected abstract void setModel();
}

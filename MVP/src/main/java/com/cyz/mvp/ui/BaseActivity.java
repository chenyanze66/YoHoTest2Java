package com.cyz.mvp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.cyz.mvp.R;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutid());
        initPresenter();

        initView(savedInstanceState);
        initData();



    }

    protected abstract int layoutid() ;

    protected abstract void initPresenter() ;
    protected abstract void initView(Bundle savedInstanceState) ;

    protected abstract void initData();
}
package com.cyz.mvp.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * 创建日期：2020/12/14 14:15
 *
 * @author cyz
 * 包名： com.cyz.mvp
 * 类说明：
 */
public abstract class BaseFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layoutid(), null);
        initPresenter();
        initView(view,savedInstanceState);
        initData();



        return view;
    }

    protected abstract int layoutid();

    protected abstract void initPresenter();

    protected abstract void initView(View view, Bundle savedInstanceState);

    protected abstract void initData() ;
}

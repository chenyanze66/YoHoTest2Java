package com.cyz.net;

/**
 * 创建日期：2020/12/14 14:05
 *
 * @author cyz
 * 包名： com.cyz.net
 * 类说明：
 */
public class TokenBean {
    public String access_token;
    public String token_type;
    public String expires_in;

}

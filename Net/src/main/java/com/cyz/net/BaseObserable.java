package com.cyz.net;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 创建日期：2020/12/14 14:02
 *
 * @author cyz
 * 包名： com.cyz.net
 * 类说明：
 */
public class BaseObserable {
    public <T>void  doIt(Observable<T> observable,BaseObserver<T> observer){
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);



    }



}

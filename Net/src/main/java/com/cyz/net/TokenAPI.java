package com.cyz.net;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * 创建日期：2020/12/14 14:01
 *
 * @author cyz
 * 包名： com.cyz.net
 * 类说明：
 */
public interface TokenAPI {

    @POST("token")
    @FormUrlEncoded
    Call<TokenBean> getToken(@Field("grant_type")String type, @Field("username")String code, @Field("password")String pwd);



}

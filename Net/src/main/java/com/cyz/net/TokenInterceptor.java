package com.cyz.net;

import com.blankj.utilcode.util.SPUtils;

import java.io.IOException;
import java.util.Objects;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 创建日期：2020/12/14 13:44
 *
 * @author cyz
 * 包名： com.cyz.net
 * 类说明：
 */
public class TokenInterceptor implements Interceptor {


    @Override
    public Response intercept(Chain chain) throws IOException {





        Request request = chain.request();
        Response response;
        if (SPUtils.getInstance().contains(BaseUrl.TOKEN_KEY)) {
            response = chain.proceed(addToken(request, SPUtils.getInstance().getString(BaseUrl.TOKEN_KEY)));
        } else {
            response = chain.proceed(request);
        }
        if (response.code()==BaseUrl.TOKEN_NON){
            String newToken=createnewToken();
            SPUtils.getInstance().put(BaseUrl.TOKEN_KEY,newToken);
            response=chain.proceed(addToken(request,newToken));

        }

        return response;
    }

    private String createnewToken() throws IOException {

        return Objects.requireNonNull(RetrofitFactory.getInstance().create(TokenAPI.class)
        .getToken("password","df1831971161731de15c18e1711ca11e12614517118d1ad1","")
        .execute().body()).access_token;
    }

    private Request addToken(Request request, String string) {
        return request.newBuilder().addHeader("Authorization", "bearer " + string).build();
    }
}

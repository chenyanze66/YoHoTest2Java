package com.cyz.yohotest2java.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.cyz.mvp.ui.BaseFragment;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.ui.LoginActivity;
import com.cyz.yohotest2java.ui.MainActivity;
import com.cyz.yohotest2java.ui.ShoppingCarActivity;

/**
 * 创建日期：2020/12/15 13:27
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.ui.fragment
 * 类说明：
 */
public class Fragment_Mine extends BaseFragment {
    private TextView textMineUserName;
    private Button shoppingcar;
    private Button unLogin;
    @Override
    protected int layoutid() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initPresenter() {

    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {


        unLogin = view.findViewById(R.id.unLogin);


        textMineUserName = view.findViewById(R.id.text_Mine_UserName);
        shoppingcar =  view.findViewById(R.id.shoppingcar);
        shoppingcar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ShoppingCarActivity.class);
                startActivity(intent);
            }
        });
        Intent intent = getActivity().getIntent();
        String username = intent.getStringExtra("username");
        if (username==null||username.equals("null")){

            String string = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE)
                    .getString("username", null);
            textMineUserName.setText(string);

        }else {

            textMineUserName.setText(""+username);

        }



    }

    @Override
    protected void initData() {
        unLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences login = getActivity().getSharedPreferences("Login", Context.MODE_PRIVATE);
                login.edit().putBoolean("login",false).commit();
                Intent intent = new Intent(getContext(),LoginActivity.class);
                startActivity(intent);
                getActivity().finish();


            }
        });
    }
}

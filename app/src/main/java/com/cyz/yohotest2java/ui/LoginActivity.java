package com.cyz.yohotest2java.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cyz.lib_core.callback.MyOkhttpCallBack;
import com.cyz.lib_core.utils.OkHttpUtils;
import com.cyz.mvp.ui.BaseActivity;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.bean.LoginBean;
import com.google.gson.Gson;

import java.util.HashMap;

public class LoginActivity extends BaseActivity {
    private EditText edLoginUsername;
    private EditText edLoginPassword;
    private Button butLogin;
    private TextView textLoginZC;


    @Override
    protected int layoutid() {
        return R.layout.activity_login;
    }

    @Override
    protected void initPresenter() {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {


        edLoginUsername = findViewById(R.id.ed_Login_username);
        edLoginPassword = findViewById(R.id.ed_Login_password);
        butLogin = findViewById(R.id.but_Login);
        textLoginZC = findViewById(R.id.text_Login___ZC);

    }

    @Override
    protected void initData() {
        textLoginZC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ZCActivity.class);
                startActivity(intent);
            }
        });

        butLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edLoginUsername.getText().toString().trim();
                String password = edLoginPassword.getText().toString().trim();
                HashMap<String, String> map = new HashMap<>();
                map.put("username", username);
                map.put("password", password);
                OkHttpUtils.getInstance().dopost("https://www.wanandroid.com/user/login", map, new MyOkhttpCallBack() {
                    @Override
                    public void onOk(String json) {
                        LoginBean loginBean = new Gson().fromJson(json, LoginBean.class);

                        if (loginBean.getErrorCode() == 0) {
                            Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra("username",username);
                            startActivity(intent);
                            SharedPreferences login = getSharedPreferences("Login", MODE_PRIVATE);
                            login.edit().putBoolean("login",true).commit();
                            login.edit().putString("username",username).commit();
                            finish();

                        } else {

                            Toast.makeText(LoginActivity.this, "" + loginBean.getErrorMsg(), Toast.LENGTH_SHORT).show();
                            edLoginPassword.setText("");
                            edLoginUsername.setText("");
                        }


                    }

                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void downloudpro(int pro) {

                    }
                });


            }
        });


    }
}
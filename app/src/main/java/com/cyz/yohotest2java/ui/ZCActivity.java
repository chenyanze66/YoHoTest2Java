package com.cyz.yohotest2java.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cyz.lib_core.callback.MyOkhttpCallBack;
import com.cyz.lib_core.utils.OkHttpUtils;
import com.cyz.mvp.ui.BaseActivity;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.bean.ZCBean;
import com.google.gson.Gson;

import java.util.HashMap;

public class ZCActivity extends BaseActivity {

    private EditText edZCUsername;
    private EditText edZCPassword;
    private EditText edZCRepassword;
    private Button butZC;
    private TextView textZCLogin;
    @Override
    protected int layoutid() {
        return R.layout.activity_z_c;
    }

    @Override
    protected void initPresenter() {

    }

    @Override
    protected void initView(Bundle savedInstanceState) {


        edZCUsername = findViewById(R.id.ed_ZC_username);
        edZCPassword = findViewById(R.id.ed_ZC_password);
        edZCRepassword = findViewById(R.id.ed_ZC_repassword);
        butZC = findViewById(R.id.but_ZC);
        textZCLogin = findViewById(R.id.text_ZC___Login);

    }

    @Override
    protected void initData() {
        textZCLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ZCActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });



            butZC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String username = edZCUsername.getText().toString().trim();
                    String password = edZCPassword.getText().toString().trim();
                    String repassword = edZCRepassword.getText().toString().trim();
                    HashMap<String, String> map = new HashMap<>();
                    map.put("username", username);
                    map.put("password", password);
                    map.put("repassword", repassword);
                    OkHttpUtils.getInstance().dopost("https://www.wanandroid.com/user/register", map, new MyOkhttpCallBack() {
                        @Override
                        public void onOk(String json) {
                            ZCBean zcBean = new Gson().fromJson(json, ZCBean.class);
                            if (zcBean.getErrorCode()==0){

                                Toast.makeText(ZCActivity.this, "注册成功,前往登录", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ZCActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }else {

                                Toast.makeText(ZCActivity.this, ""+zcBean.getErrorMsg(), Toast.LENGTH_SHORT).show();

                            }


                        }

                        @Override
                        public void onError(String message) {

                        }

                        @Override
                        public void downloudpro(int pro) {

                        }
                    });



                }
            });






    }
}
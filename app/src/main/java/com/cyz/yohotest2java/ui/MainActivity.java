package com.cyz.yohotest2java.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cyz.mvp.ui.BaseActivity;
import com.cyz.net.RetrofitFactory;

import com.cyz.versioncode_or_versionname.Get_versionCode_or_versionName;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.bean.CommBean;
import com.cyz.yohotest2java.bean.Tuijian;
import com.cyz.yohotest2java.bean.TuijianTitleBean;
import com.cyz.yohotest2java.cont.Cont;
import com.cyz.yohotest2java.presenter.M_Presenter;
import com.cyz.yohotest2java.ui.fragment.Fragment_Classify;
import com.cyz.yohotest2java.ui.fragment.Fragment_Home;
import com.cyz.yohotest2java.ui.fragment.Fragment_Mine;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends BaseActivity implements Cont.C_M_View {
    M_Presenter presenter;
    private FrameLayout frameMain;
    private Fragment_Home fragment_home;
    private Fragment_Classify fragment_classify;
    private Fragment_Mine fragment_mine;
    private CommonTabLayout comm;
    private ArrayList<CustomTabEntity> tabEntitys=new ArrayList<>();

    @Override
    protected int layoutid() {
        return R.layout.activity_main;
    }

    @Override
    protected void initPresenter() {
        presenter = new M_Presenter(this);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        int versionCode = Get_versionCode_or_versionName.getVersionCode(MainActivity.this);

        if (versionCode<5){




        }else if (versionCode==5){


        }else if (versionCode>5){

        }


        comm = (CommonTabLayout) findViewById(R.id.comm);

        fragment_home = new Fragment_Home();
        fragment_classify = new Fragment_Classify();
        fragment_mine = new Fragment_Mine();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_main,fragment_mine)
                .add(R.id.frame_main,fragment_classify)
                .add(R.id.frame_main,fragment_home)
                .commit();
        frameMain = (FrameLayout) findViewById(R.id.frame_main);
        tabEntitys.add(new CommBean("首页",R.mipmap.shouye2,R.mipmap.shouye1));
        tabEntitys.add(new CommBean("分类",R.mipmap.fenlei2,R.mipmap.fenlei1));
        tabEntitys.add(new CommBean("我的",R.mipmap.mine2,R.mipmap.mine1));

        comm.setTabData(tabEntitys);
        comm.setTextSelectColor(Color.RED);
        comm.setTextUnselectColor(Color.BLACK);

        comm.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                switch (position){
                    case 0:
                        getSupportFragmentManager().beginTransaction().hide(fragment_home)
                                .hide(fragment_classify)
                                .hide(fragment_mine)
                                .show(fragment_home)
                                .commit();


                        break;
                     case 1:
                         getSupportFragmentManager().beginTransaction().hide(fragment_home)
                                 .hide(fragment_classify)
                                 .hide(fragment_mine)
                                 .show(fragment_classify)
                                 .commit();
                        break;
                     case 2:
                         SharedPreferences login =getSharedPreferences("Login", Context.MODE_PRIVATE);
                         boolean iflogin = login.getBoolean("login", false);
                         if (!iflogin){
                             Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                             startActivity(intent);
                             getSupportFragmentManager().beginTransaction().hide(fragment_home)
                                     .hide(fragment_classify)
                                     .hide(fragment_mine)
                                     .show(fragment_home)
                                     .commit();
                             comm.setCurrentTab(0);

                         }else {
                             getSupportFragmentManager().beginTransaction().hide(fragment_home)
                                     .hide(fragment_classify)
                                     .hide(fragment_mine)
                                     .show(fragment_mine)
                                     .commit();

                         }

                        break;



                }
            }

            @Override
            public void onTabReselect(int position) {

            }
        });


    }

    @Override
    protected void initData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("page", "1");
        map.put("pagesize", "10");
        presenter.getData(map);


    }

    @Override
    public void onOk(Tuijian tuijian) {

        String s = tuijian.toString();


    }

    @Override
    public void onOk_Tuijian_Title(TuijianTitleBean tuijianTitleBean) {

    }


}
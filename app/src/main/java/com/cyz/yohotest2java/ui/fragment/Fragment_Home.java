package com.cyz.yohotest2java.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cyz.lib_core.utils.GlideUtils;
import com.cyz.mvp.ui.BaseFragment;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.adapter.Home_Goods_adapter;
import com.cyz.yohotest2java.adapter.Home_Tuijian_Title_Adapter;
import com.cyz.yohotest2java.bean.CommBean;
import com.cyz.yohotest2java.bean.Tuijian;
import com.cyz.yohotest2java.bean.TuijianTitleBean;
import com.cyz.yohotest2java.cont.Cont;
import com.cyz.yohotest2java.presenter.M_Presenter;
import com.cyz.yohotest2java.ui.XiangQingActivity;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 创建日期：2020/12/15 11:25
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.ui
 * 类说明：
 */
public class Fragment_Home extends BaseFragment implements Cont.C_M_View {
    private RecyclerView recy;
    private ArrayList<CustomTabEntity> tabEntitys = new ArrayList<>();
    private M_Presenter presenter;
    private Home_Tuijian_Title_Adapter adapter;
    private List<TuijianTitleBean.DataBean> list = new ArrayList<>();
    private RecyclerView recyHomeGoods;
    private Banner bannerHome;
    private ImageView imgHomeBig;
    private ImageView imgHomeSanGongGe1;
    private ImageView imgHomeSanGongGe2;
    private ImageView imgHomeSanGongGe3;
    private ImageView imgHomeHengxiang1;
    private ImageView imgHomeHengxiang2;
    private ImageView imgHomeHengxiang3;
    private List<String> list_Banner = new ArrayList<>();
    private Home_Goods_adapter goods_adapter;
    private List<Tuijian.DataBean> list_tuijian=new ArrayList<>();
    private SmartRefreshLayout smartHome;
    private int index=1;


    @Override
    protected int layoutid() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initPresenter() {
        presenter = new M_Presenter(this);
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {


        smartHome = view.findViewById(R.id.smart_Home);

        list_Banner.add("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1977784769,2118576209&fm=26&gp=0.jpg");
        list_Banner.add("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1977784769,2118576209&fm=26&gp=0.jpg");
        list_Banner.add("https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1977784769,2118576209&fm=26&gp=0.jpg");
        bannerHome = view.findViewById(R.id.banner_Home);
        imgHomeBig = view.findViewById(R.id.img_Home_big);
        imgHomeSanGongGe1 = view.findViewById(R.id.img_Home_SanGongGe_1);


        imgHomeHengxiang1 = view.findViewById(R.id.img_Home_hengxiang_1);




        recyHomeGoods = view.findViewById(R.id.recy_Home_Goods);

        adapter = new Home_Tuijian_Title_Adapter(R.layout.item_home_tuijiantab, list);
        recy = (RecyclerView) view.findViewById(R.id.recy);
        recy.setAdapter(adapter);

         goods_adapter = new Home_Goods_adapter(R.layout.item_tuijian_goods,list_tuijian);
         recyHomeGoods.setAdapter(goods_adapter);
         recyHomeGoods.setLayoutManager(new StaggeredGridLayoutManager(2,1));




        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setOrientation(RecyclerView.HORIZONTAL);
        recy.setLayoutManager(manager);


    }

    @Override
    protected void initData() {

        goods_adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(getContext(), XiangQingActivity.class);
                intent.putExtra("goods",list_tuijian.get(position));
                startActivity(intent);


            }
        });



        smartHome.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                HashMap<String, String> map = new HashMap<>();
                ++index;
                map.put("page",""+index);
                map.put("pagesize","20");
                presenter.getData(map);
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {

                HashMap<String, String> map = new HashMap<>();
                ++index;
                map.put("page",""+index);
                map.put("pagesize","20");
                presenter.getData(map);


            }
        });


       GlideUtils.displayImage(getContext(),"https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1977784769,2118576209&fm=26&gp=0.jpg",imgHomeBig);




        bannerHome.setImages(list_Banner);
        bannerHome.setImageLoader(new ImageLoader() {
            @Override
            public void displayImage(Context context, Object path, ImageView imageView) {
                GlideUtils.displayImage(context,(String)path,imageView);
            }
        });
        bannerHome.start();

        list_tuijian.clear();
        HashMap<String, String> map = new HashMap<>();
        map.put("page","1");
        map.put("pagesize","20");
        presenter.getData(map);


        presenter.getReommend();
    }

    @Override
    public void onOk(Tuijian tuijian) {
        List<Tuijian.DataBean> data = tuijian.getData();
        list_tuijian.addAll(data);
        goods_adapter.notifyDataSetChanged();
        smartHome.finishRefresh();
        smartHome.finishLoadMore();


    }

    @Override
    public void onOk_Tuijian_Title(TuijianTitleBean tuijianTitleBean) {
        List<TuijianTitleBean.DataBean> data = tuijianTitleBean.getData();
        list.addAll(data);

        adapter.notifyDataSetChanged();



    }
}

package com.cyz.yohotest2java.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cyz.mvp.ui.BaseFragment;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.adapter.Recy_Classify_Right_Adapter;
import com.cyz.yohotest2java.adapter.StringAdapter;
import com.cyz.yohotest2java.bean.CommBean;
import com.cyz.yohotest2java.bean.Recy_Classify_LeftBean;
import com.cyz.yohotest2java.bean.Tuijian;
import com.cyz.yohotest2java.bean.TuijianTitleBean;
import com.cyz.yohotest2java.cont.Cont;
import com.cyz.yohotest2java.presenter.M_Presenter;
import com.cyz.yohotest2java.ui.SousuoActivity;
import com.cyz.yohotest2java.ui.XiangQingActivity;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshFooter;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.RefreshState;
import com.scwang.smartrefresh.layout.listener.OnMultiPurposeListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 创建日期：2020/12/15 13:23
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.ui.fragment
 * 类说明：
 */
public class Fragment_Classify extends BaseFragment implements Cont.C_M_View {
    private RelativeLayout relayout;
    private RecyclerView recyClassifyRight;
    private RecyclerView recyClassifyLeft;
    private M_Presenter presenter;
    private StringAdapter adapter_left;
    private List<Recy_Classify_LeftBean> list_left=new ArrayList<>();
    private Recy_Classify_Right_Adapter adapter_Right_Recy;
    private List<Tuijian.DataBean> list_Right = new ArrayList<>();
    private SmartRefreshLayout smartClassify;
    private int index=1;
    @Override
    protected int layoutid() {
        return R.layout.fragment_classify;
    }

    @Override
    protected void initPresenter() {
         presenter = new M_Presenter(this);


        presenter.getReommend();
        HashMap<String, String> map = new HashMap<>();
        map.put("page","1");
        map.put("pagesize","20");
        presenter.getData(map);
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {


        smartClassify = view.findViewById(R.id.smart_Classify);

        relayout = view.findViewById(R.id.relayout_);

        adapter_left = new StringAdapter(R.layout.item_classify_left,list_left);
        recyClassifyLeft = view.findViewById(R.id.recy_Classify_left);
        recyClassifyLeft.setAdapter(adapter_left);
        recyClassifyLeft.setLayoutManager(new LinearLayoutManager(getContext()));



        adapter_Right_Recy=new Recy_Classify_Right_Adapter(R.layout.item_recy_right,list_Right);
        recyClassifyRight = view.findViewById(R.id.recy_Classify_right);
        recyClassifyRight.setAdapter(adapter_Right_Recy);
        recyClassifyRight.setLayoutManager(new GridLayoutManager(getContext(),3));


    }

    @Override
    protected void initData() {
        smartClassify.setOnMultiPurposeListener(new OnMultiPurposeListener() {
            @Override
            public void onHeaderMoving(RefreshHeader header, boolean isDragging, float percent, int offset, int headerHeight, int maxDragHeight) {
                relayout.setAlpha(1-percent);
            }

            @Override
            public void onHeaderReleased(RefreshHeader header, int headerHeight, int maxDragHeight) {

            }

            @Override
            public void onHeaderStartAnimator(RefreshHeader header, int headerHeight, int maxDragHeight) {

            }

            @Override
            public void onHeaderFinish(RefreshHeader header, boolean success) {

            }

            @Override
            public void onFooterMoving(RefreshFooter footer, boolean isDragging, float percent, int offset, int footerHeight, int maxDragHeight) {

            }

            @Override
            public void onFooterReleased(RefreshFooter footer, int footerHeight, int maxDragHeight) {

            }

            @Override
            public void onFooterStartAnimator(RefreshFooter footer, int footerHeight, int maxDragHeight) {

            }

            @Override
            public void onFooterFinish(RefreshFooter footer, boolean success) {

            }

            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {

            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {

            }

            @Override
            public void onStateChanged(@NonNull RefreshLayout refreshLayout, @NonNull RefreshState oldState, @NonNull RefreshState newState) {

            }
        });


        smartClassify.setOnRefreshListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {

            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                HashMap<String, String> map = new HashMap<>();
                map.put("page","1");
                map.put("pagesize","20");
                presenter.getData(map);

            }
        });



        relayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new  Intent(getContext(), SousuoActivity.class));
            }
        });

        adapter_Right_Recy.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent = new Intent(getContext(), XiangQingActivity.class);
                intent.putExtra("goods",list_Right.get(position));
                startActivity(intent);


            }
        });
        adapter_left.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                list_Right.clear();
                for (Recy_Classify_LeftBean recy_classify_leftBean : list_left) {
                    recy_classify_leftBean.setFlag(false);
                }
                list_left.get(position).setFlag(true);

                adapter_left.notifyDataSetChanged();
                if (position+1==index){


                }else {
                    HashMap<String, String> map = new HashMap<>();
                    index=position+1;
                    map.put("page",""+index);
                    map.put("pagesize","20");
                    presenter.getData(map);
                }




            }
        });



    }

    @Override
    public void onOk(Tuijian tuijian) {
        list_Right.addAll(tuijian.getData());
        adapter_Right_Recy.notifyDataSetChanged();
        smartClassify.finishRefresh();



    }

    @Override
    public void onOk_Tuijian_Title(TuijianTitleBean tuijianTitleBean) {
        for (TuijianTitleBean.DataBean datum : tuijianTitleBean.getData()) {

            list_left.add(new Recy_Classify_LeftBean(datum.getCategoryName()));


        }
        list_left.get(0).setFlag(true);
        adapter_left.notifyDataSetChanged();


    }
}

package com.cyz.yohotest2java.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cyz.lib_core.utils.GlideUtils;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.bean.Tuijian;

import java.util.List;

/**
 * 创建日期：2020/12/17 14:32
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.adapter
 * 类说明：
 */
public class Recy_Classify_Right_Adapter extends BaseQuickAdapter<Tuijian.DataBean, BaseViewHolder> {


    public Recy_Classify_Right_Adapter(int layoutResId, @Nullable List<Tuijian.DataBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Tuijian.DataBean item) {
        helper.setText(R.id.text_Classify_Right,item.getTitle());
        GlideUtils.displayImage(mContext,item.getPictUrl(),helper.getView(R.id.img_Classify_Right));



    }
}

package com.cyz.yohotest2java.adapter;

import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.LogUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cyz.lib_core.utils.GlideUtils;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.bean.GoodsBean;

import java.util.List;

/**
 * 创建日期：2020/12/24 13:22
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.adapter
 * 类说明：
 */
public class ShoppingCar_Adapter extends BaseQuickAdapter<GoodsBean, BaseViewHolder> {
    public ShoppingCar_Adapter(int layoutResId, @Nullable List<GoodsBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, GoodsBean item) {

        CheckBox view = helper.getView(R.id.check_item_Shopping);
        view.setChecked(item.getFlag());
        helper.setText(R.id.title_item_Shopping,item.getTitle());
        GlideUtils.displayImage(mContext,item.getImg_pic_url(),helper.getView(R.id.img_item_Shopping));



        helper.setText(R.id.price_item_Shopping,"￥"+item.getPrice());
        helper.setText(R.id.but_item_Shopping_sum,item.getIndex()+"");
        helper.addOnClickListener(R.id.check_item_Shopping)
                .addOnClickListener(R.id.but_item_Shopping_low)
                .addOnClickListener(R.id.but_item_Shopping_up);
    }
}

package com.cyz.yohotest2java.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.bean.TuijianTitleBean;

import java.util.List;

import io.reactivex.internal.observers.BasicQueueDisposable;

/**
 * 创建日期：2020/12/16 10:37
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.adapter
 * 类说明：
 */
public class Home_Tuijian_Title_Adapter extends BaseQuickAdapter<TuijianTitleBean.DataBean, BaseViewHolder> {


    public Home_Tuijian_Title_Adapter(int layoutResId, @Nullable List<TuijianTitleBean.DataBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TuijianTitleBean.DataBean item) {
        helper.setText(R.id.text_Tuijiantab,item.getCategoryName());
    }
}

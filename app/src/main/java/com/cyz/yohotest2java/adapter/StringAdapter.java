package com.cyz.yohotest2java.adapter;

import android.graphics.Typeface;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.bean.Recy_Classify_LeftBean;

import java.util.List;

/**
 * 创建日期：2020/12/17 10:45
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.adapter
 * 类说明：
 */
public class StringAdapter extends BaseQuickAdapter<Recy_Classify_LeftBean, BaseViewHolder> {

    public StringAdapter(int layoutResId, @Nullable List<Recy_Classify_LeftBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Recy_Classify_LeftBean item) {
        TextView textView = helper.getView(R.id.text_String);
        textView.setText(item.getName());



        LinearLayout view = helper.getView(R.id.linemy);
        if (item.isFlag()){
            view.setVisibility(View.VISIBLE);
            textView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            textView.setTextSize(9);
        }else {
            view.setVisibility(View.INVISIBLE);
            textView.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
            textView.setTextSize(8);
        }

    }
}

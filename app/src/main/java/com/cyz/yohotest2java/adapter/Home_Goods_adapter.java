package com.cyz.yohotest2java.adapter;

import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cyz.lib_core.utils.GlideUtils;
import com.cyz.yohotest2java.R;
import com.cyz.yohotest2java.bean.Tuijian;

import java.util.List;

/**
 * 创建日期：2020/12/16 15:58
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.adapter
 * 类说明：
 */
public class Home_Goods_adapter extends BaseQuickAdapter<Tuijian.DataBean, BaseViewHolder> {


    public Home_Goods_adapter(int layoutResId, @Nullable List<Tuijian.DataBean> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Tuijian.DataBean item) {
        helper.setText(R.id.text_item_tuijian_goods_details,item.getTitle());
        helper.setText(R.id.text_item_tuijian_goods_price,"￥"+item.getReservePrice());
        RequestOptions options = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).dontAnimate();
//        Glide.with(mContext).load(item.getPictUrl()).apply(options).into((ImageView) helper.getView(R.id.img_item_tuijian_goods));
        GlideUtils.displayImage(mContext,item.getPictUrl(),helper.getView(R.id.img_item_tuijian_goods));


    }
}

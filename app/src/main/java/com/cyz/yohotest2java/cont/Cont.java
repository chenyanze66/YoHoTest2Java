package com.cyz.yohotest2java.cont;

import com.cyz.mvp.model.IModel;
import com.cyz.mvp.presenter.BasePresenter;
import com.cyz.mvp.ui.IView;
import com.cyz.yohotest2java.bean.Tuijian;
import com.cyz.yohotest2java.bean.TuijianTitleBean;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * 创建日期：2020/12/14 14:24
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.cont
 * 类说明：
 */
public interface Cont {
    interface C_M_Model extends IModel {
        Observable<Tuijian>getData(HashMap<String ,String> map);
       Observable<TuijianTitleBean> getReommend();
    }
    abstract class C_M_Presenter extends BasePresenter<C_M_Model,C_M_View> {
        public C_M_Presenter(C_M_View _View) {
            super(_View);
        }

        @Override
        protected abstract void setModel();
        protected abstract void getData(HashMap<String, String> map);
        protected abstract void getReommend();

    }

    interface C_M_View extends IView {
        void onOk(Tuijian tuijian);
        void onOk_Tuijian_Title(TuijianTitleBean tuijianTitleBean);


    }



}

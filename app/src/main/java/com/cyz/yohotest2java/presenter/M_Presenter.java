package com.cyz.yohotest2java.presenter;

import android.util.Log;

import com.cyz.net.BaseObserable;
import com.cyz.net.BaseObserver;
import com.cyz.yohotest2java.bean.Tuijian;
import com.cyz.yohotest2java.bean.TuijianTitleBean;
import com.cyz.yohotest2java.cont.Cont;
import com.cyz.yohotest2java.model.M_Model;

import java.util.HashMap;

import io.reactivex.annotations.NonNull;

/**
 * 创建日期：2020/12/14 14:28
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.presenter
 * 类说明：
 */
public class M_Presenter extends Cont.C_M_Presenter {
    public M_Presenter(Cont.C_M_View _View) {
        super(_View);
    }

    @Override
    protected void setModel() {
        mModel = new M_Model();
    }

    @Override
    public void getData(HashMap<String, String> map) {
        new BaseObserable().doIt(mModel.getData(map),new BaseObserver<Tuijian>(){


            @Override
            public void onNext(@NonNull Tuijian tuijian) {
                Log.e("cyz",""+tuijian.getCode());
                mView.get().onOk(tuijian);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });




    }

    @Override
    public void getReommend() {
        new BaseObserable().doIt(mModel.getReommend(), new BaseObserver<TuijianTitleBean>() {
            @Override
            public void onNext(@NonNull TuijianTitleBean tuijianTitleBean) {
                mView.get().onOk_Tuijian_Title(tuijianTitleBean);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }
        });
    }
}

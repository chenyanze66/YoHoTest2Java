package com.cyz.yohotest2java.bean;

import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Unique;
import org.greenrobot.greendao.annotation.Generated;

import java.io.Serializable;

/**
 * 创建日期：2020/12/24 11:15
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java
 * 类说明：
 */
@Entity
public class GoodsBean {


    @Id(autoincrement = true)
    @Unique
    private Long id;
    @Property
    private String title;
    @Property
    private String  price;
    @Property
    private String img_pic_url;
    @Property
    private boolean flag;
    @Property
    private int index;

    @Keep
    public GoodsBean(Long id, String title, String  price, String img_pic_url) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.img_pic_url = img_pic_url;
    }
    @Generated(hash = 1806305570)
    public GoodsBean() {
    }
    @Generated(hash = 1244163397)
    public GoodsBean(Long id, String title, String price, String img_pic_url,
            boolean flag, int index) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.img_pic_url = img_pic_url;
        this.flag = flag;
        this.index = index;
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String  getPrice() {
        return this.price;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    public String getImg_pic_url() {
        return this.img_pic_url;
    }
    public void setImg_pic_url(String img_pic_url) {
        this.img_pic_url = img_pic_url;
    }

    @Override
    public String toString() {
        return "GoodsBean{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", price='" + price + '\'' +
                ", img_pic_url='" + img_pic_url + '\'' +
                '}';
    }
    public boolean getFlag() {
        return this.flag;
    }
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    public int getIndex() {
        return this.index;
    }
    public void setIndex(int index) {
        this.index = index;
    }
}

package com.cyz.yohotest2java.bean;

/**
 * 创建日期：2020/12/17 11:06
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.bean
 * 类说明：
 */
public class Recy_Classify_LeftBean {
    String name;
    boolean flag;

    public Recy_Classify_LeftBean(String name) {
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}

package com.cyz.yohotest2java.bean;

import com.flyco.tablayout.listener.CustomTabEntity;

/**
 * 创建日期：2020/12/16 9:31
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.bean
 * 类说明：
 */
public class CommBean implements CustomTabEntity {
    String title;
    int selectedIcon;
    int unselectedIcon;

    public CommBean(String title, int selectedIcon, int unselectedIcon) {
        this.title = title;
        this.selectedIcon = selectedIcon;
        this.unselectedIcon = unselectedIcon;
    }

    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public int getTabSelectedIcon() {
        return selectedIcon;
    }

    @Override
    public int getTabUnselectedIcon() {
        return unselectedIcon;
    }
}

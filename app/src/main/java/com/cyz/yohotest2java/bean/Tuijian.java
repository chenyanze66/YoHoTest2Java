package com.cyz.yohotest2java.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * 创建日期：2020/12/14 14:32
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.bean
 * 类说明：
 */
public class Tuijian {

    @SerializedName("code")
    private Integer code;
    @SerializedName("msg")
    private String msg;
    @SerializedName("data")
    private List<DataBean> data;

    @Override
    public String toString() {
        return "Tuijian{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        @Override
        public String toString() {
            return "DataBean{" +
                    "CategoryId=" + CategoryId +
                    ", CategoryName='" + CategoryName + '\'' +
                    ", CommissionRate='" + CommissionRate + '\'' +
                    ", CommissionType='" + CommissionType + '\'' +
                    ", CouponAmount='" + CouponAmount + '\'' +
                    ", CouponEndTime='" + CouponEndTime + '\'' +
                    ", CouponId='" + CouponId + '\'' +
                    ", CouponInfo='" + CouponInfo + '\'' +
                    ", CouponRemainCount=" + CouponRemainCount +
                    ", CouponShareUrl='" + CouponShareUrl + '\'' +
                    ", CouponStartFee='" + CouponStartFee + '\'' +
                    ", CouponStartTime='" + CouponStartTime + '\'' +
                    ", CouponTotalCount=" + CouponTotalCount +
                    ", Distance=" + Distance +
                    ", IncludeDxjh='" + IncludeDxjh + '\'' +
                    ", IncludeMkt='" + IncludeMkt + '\'' +
                    ", InfoDxjh='" + InfoDxjh + '\'' +
                    ", ItemDescription='" + ItemDescription + '\'' +
                    ", ItemId=" + ItemId +
                    ", ItemUrl='" + ItemUrl + '\'' +
                    ", JddNum=" + JddNum +
                    ", JddPrice=" + JddPrice +
                    ", KuadianPromotionInfo=" + KuadianPromotionInfo +
                    ", LevelOneCategoryId=" + LevelOneCategoryId +
                    ", LevelOneCategoryName='" + LevelOneCategoryName + '\'' +
                    ", LockRate=" + LockRate +
                    ", LockRateEndTime=" + LockRateEndTime +
                    ", LockRateStartTime=" + LockRateStartTime +
                    ", Nick='" + Nick + '\'' +
                    ", NumIid=" + NumIid +
                    ", Oetime=" + Oetime +
                    ", OrigPrice=" + OrigPrice +
                    ", Ostime=" + Ostime +
                    ", PictUrl='" + PictUrl + '\'' +
                    ", PresaleDeposit='" + PresaleDeposit + '\'' +
                    ", PresaleDiscountFeeText=" + PresaleDiscountFeeText +
                    ", PresaleEndTime=" + PresaleEndTime +
                    ", PresaleStartTime=" + PresaleStartTime +
                    ", PresaleTailEndTime=" + PresaleTailEndTime +
                    ", PresaleTailStartTime=" + PresaleTailStartTime +
                    ", Provcity='" + Provcity + '\'' +
                    ", RealPostFee='" + RealPostFee + '\'' +
                    ", ReservePrice='" + ReservePrice + '\'' +
                    ", SaleBeginTime=" + SaleBeginTime +
                    ", SaleEndTime=" + SaleEndTime +
                    ", SalePrice=" + SalePrice +
                    ", SellNum=" + SellNum +
                    ", SellerId=" + SellerId +
                    ", ShopDsr=" + ShopDsr +
                    ", ShopTitle='" + ShopTitle + '\'' +
                    ", ShortTitle='" + ShortTitle + '\'' +
                    ", Stock=" + Stock +
                    ", Title='" + Title + '\'' +
                    ", TkTotalCommi='" + TkTotalCommi + '\'' +
                    ", TkTotalSales='" + TkTotalSales + '\'' +
                    ", TmallPlayActivityInfo=" + TmallPlayActivityInfo +
                    ", TotalStock=" + TotalStock +
                    ", Url='" + Url + '\'' +
                    ", UsableShopId=" + UsableShopId +
                    ", UsableShopName=" + UsableShopName +
                    ", UserType=" + UserType +
                    ", UvSumPreSale=" + UvSumPreSale +
                    ", Volume=" + Volume +
                    ", WhiteImage='" + WhiteImage + '\'' +
                    ", XId='" + XId + '\'' +
                    ", YsylClickUrl=" + YsylClickUrl +
                    ", YsylCommissionRate=" + YsylCommissionRate +
                    ", YsylTljFace=" + YsylTljFace +
                    ", YsylTljSendTime=" + YsylTljSendTime +
                    ", YsylTljUseEndTime=" + YsylTljUseEndTime +
                    ", YsylTljUseStartTime=" + YsylTljUseStartTime +
                    ", ZkFinalPrice='" + ZkFinalPrice + '\'' +
                    ", SmallImages=" + SmallImages +
                    '}';
        }

        @SerializedName("CategoryId")
        private Integer CategoryId;
        @SerializedName("CategoryName")
        private String CategoryName;
        @SerializedName("CommissionRate")
        private String CommissionRate;
        @SerializedName("CommissionType")
        private String CommissionType;
        @SerializedName("CouponAmount")
        private String CouponAmount;
        @SerializedName("CouponEndTime")
        private String CouponEndTime;
        @SerializedName("CouponId")
        private String CouponId;
        @SerializedName("CouponInfo")
        private String CouponInfo;
        @SerializedName("CouponRemainCount")
        private Integer CouponRemainCount;
        @SerializedName("CouponShareUrl")
        private String CouponShareUrl;
        @SerializedName("CouponStartFee")
        private String CouponStartFee;
        @SerializedName("CouponStartTime")
        private String CouponStartTime;
        @SerializedName("CouponTotalCount")
        private Integer CouponTotalCount;
        @SerializedName("Distance")
        private Object Distance;
        @SerializedName("IncludeDxjh")
        private String IncludeDxjh;
        @SerializedName("IncludeMkt")
        private String IncludeMkt;
        @SerializedName("InfoDxjh")
        private String InfoDxjh;
        @SerializedName("ItemDescription")
        private String ItemDescription;
        @SerializedName("ItemId")
        private Long ItemId;
        @SerializedName("ItemUrl")
        private String ItemUrl;
        @SerializedName("JddNum")
        private Integer JddNum;
        @SerializedName("JddPrice")
        private Object JddPrice;
        @SerializedName("KuadianPromotionInfo")
        private Object KuadianPromotionInfo;
        @SerializedName("LevelOneCategoryId")
        private Integer LevelOneCategoryId;
        @SerializedName("LevelOneCategoryName")
        private String LevelOneCategoryName;
        @SerializedName("LockRate")
        private Object LockRate;
        @SerializedName("LockRateEndTime")
        private Integer LockRateEndTime;
        @SerializedName("LockRateStartTime")
        private Integer LockRateStartTime;
        @SerializedName("Nick")
        private String Nick;
        @SerializedName("NumIid")
        private Long NumIid;
        @SerializedName("Oetime")
        private Object Oetime;
        @SerializedName("OrigPrice")
        private Object OrigPrice;
        @SerializedName("Ostime")
        private Object Ostime;
        @SerializedName("PictUrl")
        private String PictUrl;
        @SerializedName("PresaleDeposit")
        private String PresaleDeposit;
        @SerializedName("PresaleDiscountFeeText")
        private Object PresaleDiscountFeeText;
        @SerializedName("PresaleEndTime")
        private Integer PresaleEndTime;
        @SerializedName("PresaleStartTime")
        private Integer PresaleStartTime;
        @SerializedName("PresaleTailEndTime")
        private Integer PresaleTailEndTime;
        @SerializedName("PresaleTailStartTime")
        private Integer PresaleTailStartTime;
        @SerializedName("Provcity")
        private String Provcity;
        @SerializedName("RealPostFee")
        private String RealPostFee;
        @SerializedName("ReservePrice")
        private String ReservePrice;
        @SerializedName("SaleBeginTime")
        private Object SaleBeginTime;
        @SerializedName("SaleEndTime")
        private Object SaleEndTime;
        @SerializedName("SalePrice")
        private Object SalePrice;
        @SerializedName("SellNum")
        private Integer SellNum;
        @SerializedName("SellerId")
        private Long SellerId;
        @SerializedName("ShopDsr")
        private Integer ShopDsr;
        @SerializedName("ShopTitle")
        private String ShopTitle;
        @SerializedName("ShortTitle")
        private String ShortTitle;
        @SerializedName("Stock")
        private Integer Stock;
        @SerializedName("Title")
        private String Title;
        @SerializedName("TkTotalCommi")
        private String TkTotalCommi;
        @SerializedName("TkTotalSales")
        private String TkTotalSales;
        @SerializedName("TmallPlayActivityInfo")
        private Object TmallPlayActivityInfo;
        @SerializedName("TotalStock")
        private Integer TotalStock;
        @SerializedName("Url")
        private String Url;
        @SerializedName("UsableShopId")
        private Object UsableShopId;
        @SerializedName("UsableShopName")
        private Object UsableShopName;
        @SerializedName("UserType")
        private Integer UserType;
        @SerializedName("UvSumPreSale")
        private Integer UvSumPreSale;
        @SerializedName("Volume")
        private Integer Volume;
        @SerializedName("WhiteImage")
        private String WhiteImage;
        @SerializedName("XId")
        private String XId;
        @SerializedName("YsylClickUrl")
        private Object YsylClickUrl;
        @SerializedName("YsylCommissionRate")
        private Object YsylCommissionRate;
        @SerializedName("YsylTljFace")
        private Object YsylTljFace;
        @SerializedName("YsylTljSendTime")
        private Object YsylTljSendTime;
        @SerializedName("YsylTljUseEndTime")
        private Object YsylTljUseEndTime;
        @SerializedName("YsylTljUseStartTime")
        private Object YsylTljUseStartTime;
        @SerializedName("ZkFinalPrice")
        private String ZkFinalPrice;
        @SerializedName("SmallImages")
        private List<String> SmallImages;

        public Integer getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(Integer CategoryId) {
            this.CategoryId = CategoryId;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(String CategoryName) {
            this.CategoryName = CategoryName;
        }

        public String getCommissionRate() {
            return CommissionRate;
        }

        public void setCommissionRate(String CommissionRate) {
            this.CommissionRate = CommissionRate;
        }

        public String getCommissionType() {
            return CommissionType;
        }

        public void setCommissionType(String CommissionType) {
            this.CommissionType = CommissionType;
        }

        public String getCouponAmount() {
            return CouponAmount;
        }

        public void setCouponAmount(String CouponAmount) {
            this.CouponAmount = CouponAmount;
        }

        public String getCouponEndTime() {
            return CouponEndTime;
        }

        public void setCouponEndTime(String CouponEndTime) {
            this.CouponEndTime = CouponEndTime;
        }

        public String getCouponId() {
            return CouponId;
        }

        public void setCouponId(String CouponId) {
            this.CouponId = CouponId;
        }

        public String getCouponInfo() {
            return CouponInfo;
        }

        public void setCouponInfo(String CouponInfo) {
            this.CouponInfo = CouponInfo;
        }

        public Integer getCouponRemainCount() {
            return CouponRemainCount;
        }

        public void setCouponRemainCount(Integer CouponRemainCount) {
            this.CouponRemainCount = CouponRemainCount;
        }

        public String getCouponShareUrl() {
            return CouponShareUrl;
        }

        public void setCouponShareUrl(String CouponShareUrl) {
            this.CouponShareUrl = CouponShareUrl;
        }

        public String getCouponStartFee() {
            return CouponStartFee;
        }

        public void setCouponStartFee(String CouponStartFee) {
            this.CouponStartFee = CouponStartFee;
        }

        public String getCouponStartTime() {
            return CouponStartTime;
        }

        public void setCouponStartTime(String CouponStartTime) {
            this.CouponStartTime = CouponStartTime;
        }

        public Integer getCouponTotalCount() {
            return CouponTotalCount;
        }

        public void setCouponTotalCount(Integer CouponTotalCount) {
            this.CouponTotalCount = CouponTotalCount;
        }

        public Object getDistance() {
            return Distance;
        }

        public void setDistance(Object Distance) {
            this.Distance = Distance;
        }

        public String getIncludeDxjh() {
            return IncludeDxjh;
        }

        public void setIncludeDxjh(String IncludeDxjh) {
            this.IncludeDxjh = IncludeDxjh;
        }

        public String getIncludeMkt() {
            return IncludeMkt;
        }

        public void setIncludeMkt(String IncludeMkt) {
            this.IncludeMkt = IncludeMkt;
        }

        public String getInfoDxjh() {
            return InfoDxjh;
        }

        public void setInfoDxjh(String InfoDxjh) {
            this.InfoDxjh = InfoDxjh;
        }

        public String getItemDescription() {
            return ItemDescription;
        }

        public void setItemDescription(String ItemDescription) {
            this.ItemDescription = ItemDescription;
        }

        public Long getItemId() {
            return ItemId;
        }

        public void setItemId(Long ItemId) {
            this.ItemId = ItemId;
        }

        public String getItemUrl() {
            return ItemUrl;
        }

        public void setItemUrl(String ItemUrl) {
            this.ItemUrl = ItemUrl;
        }

        public Integer getJddNum() {
            return JddNum;
        }

        public void setJddNum(Integer JddNum) {
            this.JddNum = JddNum;
        }

        public Object getJddPrice() {
            return JddPrice;
        }

        public void setJddPrice(Object JddPrice) {
            this.JddPrice = JddPrice;
        }

        public Object getKuadianPromotionInfo() {
            return KuadianPromotionInfo;
        }

        public void setKuadianPromotionInfo(Object KuadianPromotionInfo) {
            this.KuadianPromotionInfo = KuadianPromotionInfo;
        }

        public Integer getLevelOneCategoryId() {
            return LevelOneCategoryId;
        }

        public void setLevelOneCategoryId(Integer LevelOneCategoryId) {
            this.LevelOneCategoryId = LevelOneCategoryId;
        }

        public String getLevelOneCategoryName() {
            return LevelOneCategoryName;
        }

        public void setLevelOneCategoryName(String LevelOneCategoryName) {
            this.LevelOneCategoryName = LevelOneCategoryName;
        }

        public Object getLockRate() {
            return LockRate;
        }

        public void setLockRate(Object LockRate) {
            this.LockRate = LockRate;
        }

        public Integer getLockRateEndTime() {
            return LockRateEndTime;
        }

        public void setLockRateEndTime(Integer LockRateEndTime) {
            this.LockRateEndTime = LockRateEndTime;
        }

        public Integer getLockRateStartTime() {
            return LockRateStartTime;
        }

        public void setLockRateStartTime(Integer LockRateStartTime) {
            this.LockRateStartTime = LockRateStartTime;
        }

        public String getNick() {
            return Nick;
        }

        public void setNick(String Nick) {
            this.Nick = Nick;
        }

        public Long getNumIid() {
            return NumIid;
        }

        public void setNumIid(Long NumIid) {
            this.NumIid = NumIid;
        }

        public Object getOetime() {
            return Oetime;
        }

        public void setOetime(Object Oetime) {
            this.Oetime = Oetime;
        }

        public Object getOrigPrice() {
            return OrigPrice;
        }

        public void setOrigPrice(Object OrigPrice) {
            this.OrigPrice = OrigPrice;
        }

        public Object getOstime() {
            return Ostime;
        }

        public void setOstime(Object Ostime) {
            this.Ostime = Ostime;
        }

        public String getPictUrl() {
            return PictUrl;
        }

        public void setPictUrl(String PictUrl) {
            this.PictUrl = PictUrl;
        }

        public String getPresaleDeposit() {
            return PresaleDeposit;
        }

        public void setPresaleDeposit(String PresaleDeposit) {
            this.PresaleDeposit = PresaleDeposit;
        }

        public Object getPresaleDiscountFeeText() {
            return PresaleDiscountFeeText;
        }

        public void setPresaleDiscountFeeText(Object PresaleDiscountFeeText) {
            this.PresaleDiscountFeeText = PresaleDiscountFeeText;
        }

        public Integer getPresaleEndTime() {
            return PresaleEndTime;
        }

        public void setPresaleEndTime(Integer PresaleEndTime) {
            this.PresaleEndTime = PresaleEndTime;
        }

        public Integer getPresaleStartTime() {
            return PresaleStartTime;
        }

        public void setPresaleStartTime(Integer PresaleStartTime) {
            this.PresaleStartTime = PresaleStartTime;
        }

        public Integer getPresaleTailEndTime() {
            return PresaleTailEndTime;
        }

        public void setPresaleTailEndTime(Integer PresaleTailEndTime) {
            this.PresaleTailEndTime = PresaleTailEndTime;
        }

        public Integer getPresaleTailStartTime() {
            return PresaleTailStartTime;
        }

        public void setPresaleTailStartTime(Integer PresaleTailStartTime) {
            this.PresaleTailStartTime = PresaleTailStartTime;
        }

        public String getProvcity() {
            return Provcity;
        }

        public void setProvcity(String Provcity) {
            this.Provcity = Provcity;
        }

        public String getRealPostFee() {
            return RealPostFee;
        }

        public void setRealPostFee(String RealPostFee) {
            this.RealPostFee = RealPostFee;
        }

        public String getReservePrice() {
            return ReservePrice;
        }

        public void setReservePrice(String ReservePrice) {
            this.ReservePrice = ReservePrice;
        }

        public Object getSaleBeginTime() {
            return SaleBeginTime;
        }

        public void setSaleBeginTime(Object SaleBeginTime) {
            this.SaleBeginTime = SaleBeginTime;
        }

        public Object getSaleEndTime() {
            return SaleEndTime;
        }

        public void setSaleEndTime(Object SaleEndTime) {
            this.SaleEndTime = SaleEndTime;
        }

        public Object getSalePrice() {
            return SalePrice;
        }

        public void setSalePrice(Object SalePrice) {
            this.SalePrice = SalePrice;
        }

        public Integer getSellNum() {
            return SellNum;
        }

        public void setSellNum(Integer SellNum) {
            this.SellNum = SellNum;
        }

        public Long getSellerId() {
            return SellerId;
        }

        public void setSellerId(Long SellerId) {
            this.SellerId = SellerId;
        }

        public Integer getShopDsr() {
            return ShopDsr;
        }

        public void setShopDsr(Integer ShopDsr) {
            this.ShopDsr = ShopDsr;
        }

        public String getShopTitle() {
            return ShopTitle;
        }

        public void setShopTitle(String ShopTitle) {
            this.ShopTitle = ShopTitle;
        }

        public String getShortTitle() {
            return ShortTitle;
        }

        public void setShortTitle(String ShortTitle) {
            this.ShortTitle = ShortTitle;
        }

        public Integer getStock() {
            return Stock;
        }

        public void setStock(Integer Stock) {
            this.Stock = Stock;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getTkTotalCommi() {
            return TkTotalCommi;
        }

        public void setTkTotalCommi(String TkTotalCommi) {
            this.TkTotalCommi = TkTotalCommi;
        }

        public String getTkTotalSales() {
            return TkTotalSales;
        }

        public void setTkTotalSales(String TkTotalSales) {
            this.TkTotalSales = TkTotalSales;
        }

        public Object getTmallPlayActivityInfo() {
            return TmallPlayActivityInfo;
        }

        public void setTmallPlayActivityInfo(Object TmallPlayActivityInfo) {
            this.TmallPlayActivityInfo = TmallPlayActivityInfo;
        }

        public Integer getTotalStock() {
            return TotalStock;
        }

        public void setTotalStock(Integer TotalStock) {
            this.TotalStock = TotalStock;
        }

        public String getUrl() {
            return Url;
        }

        public void setUrl(String Url) {
            this.Url = Url;
        }

        public Object getUsableShopId() {
            return UsableShopId;
        }

        public void setUsableShopId(Object UsableShopId) {
            this.UsableShopId = UsableShopId;
        }

        public Object getUsableShopName() {
            return UsableShopName;
        }

        public void setUsableShopName(Object UsableShopName) {
            this.UsableShopName = UsableShopName;
        }

        public Integer getUserType() {
            return UserType;
        }

        public void setUserType(Integer UserType) {
            this.UserType = UserType;
        }

        public Integer getUvSumPreSale() {
            return UvSumPreSale;
        }

        public void setUvSumPreSale(Integer UvSumPreSale) {
            this.UvSumPreSale = UvSumPreSale;
        }

        public Integer getVolume() {
            return Volume;
        }

        public void setVolume(Integer Volume) {
            this.Volume = Volume;
        }

        public String getWhiteImage() {
            return WhiteImage;
        }

        public void setWhiteImage(String WhiteImage) {
            this.WhiteImage = WhiteImage;
        }

        public String getXId() {
            return XId;
        }

        public void setXId(String XId) {
            this.XId = XId;
        }

        public Object getYsylClickUrl() {
            return YsylClickUrl;
        }

        public void setYsylClickUrl(Object YsylClickUrl) {
            this.YsylClickUrl = YsylClickUrl;
        }

        public Object getYsylCommissionRate() {
            return YsylCommissionRate;
        }

        public void setYsylCommissionRate(Object YsylCommissionRate) {
            this.YsylCommissionRate = YsylCommissionRate;
        }

        public Object getYsylTljFace() {
            return YsylTljFace;
        }

        public void setYsylTljFace(Object YsylTljFace) {
            this.YsylTljFace = YsylTljFace;
        }

        public Object getYsylTljSendTime() {
            return YsylTljSendTime;
        }

        public void setYsylTljSendTime(Object YsylTljSendTime) {
            this.YsylTljSendTime = YsylTljSendTime;
        }

        public Object getYsylTljUseEndTime() {
            return YsylTljUseEndTime;
        }

        public void setYsylTljUseEndTime(Object YsylTljUseEndTime) {
            this.YsylTljUseEndTime = YsylTljUseEndTime;
        }

        public Object getYsylTljUseStartTime() {
            return YsylTljUseStartTime;
        }

        public void setYsylTljUseStartTime(Object YsylTljUseStartTime) {
            this.YsylTljUseStartTime = YsylTljUseStartTime;
        }

        public String getZkFinalPrice() {
            return ZkFinalPrice;
        }

        public void setZkFinalPrice(String ZkFinalPrice) {
            this.ZkFinalPrice = ZkFinalPrice;
        }

        public List<String> getSmallImages() {
            return SmallImages;
        }

        public void setSmallImages(List<String> SmallImages) {
            this.SmallImages = SmallImages;
        }
    }
}

package com.cyz.yohotest2java;

import android.app.Application;

import com.alipay.sdk.app.EnvUtils;

/**
 * 创建日期：2020/12/22 11:04
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java
 * 类说明：
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        EnvUtils.setEnv(EnvUtils.EnvEnum.SANDBOX);


    }
}

package com.cyz.yohotest2java.model;

import com.cyz.yohotest2java.bean.Tuijian;
import com.cyz.yohotest2java.bean.TuijianTitleBean;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * 创建日期：2020/12/14 14:37
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.model
 * 类说明：
 */
public interface API {
    @GET("api/Goods/getRecommendGoods")
    Observable<Tuijian> getData( @QueryMap HashMap<String,String> map);


 @GET("api/GoodsType/getRecommendTypes")
    Observable<TuijianTitleBean> getReommend();

}

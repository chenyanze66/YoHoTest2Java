package com.cyz.yohotest2java.model;

import com.cyz.net.RetrofitFactory;
import com.cyz.yohotest2java.bean.Tuijian;
import com.cyz.yohotest2java.bean.TuijianTitleBean;
import com.cyz.yohotest2java.cont.Cont;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * 创建日期：2020/12/14 14:28
 *
 * @author cyz
 * 包名： com.cyz.yohotest2java.model
 * 类说明：
 */
public class M_Model implements Cont.C_M_Model {


    @Override
    public Observable<Tuijian> getData(HashMap<String, String> map) {
        API api = RetrofitFactory.getInstance().create(API.class);



        return api.getData(map);
    }

    @Override
    public Observable<TuijianTitleBean> getReommend() {
        API api = RetrofitFactory.getInstance().create(API.class);

        return api.getReommend();
    }
}
